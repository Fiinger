/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Soft-
 * ware"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, provided that the above copyright
 * notice(s) and this permission notice appear in all copies of the Soft-
 * ware and that both the above copyright notice(s) and this permission
 * notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
 * ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY
 * RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN
 * THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSE-
 * QUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFOR-
 * MANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder shall
 * not be used in advertising or otherwise to promote the sale, use or
 * other dealings in this Software without prior written authorization of
 * the copyright holder.
 *
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <X11/extensions/XIproto.h>
#include <xorg/xf86.h>
#include <xorg/xf86_OSlib.h>
#include <xorg/xf86Xinput.h>

#include "fiinger.h"

/* module */
static void     FiingerUnplug(pointer p);
static pointer  FiingerPlug(pointer        module,
                           pointer        options,
                           int            *errmaj,
                           int            *errmin);

/* driver */
static InputInfoPtr FiingerPreInit(InputDriverPtr  drv, 
                                  IDevPtr         dev, 
                                  int             flags);

static void         FiingerUnInit(InputDriverPtr   drv,
                                 InputInfoPtr     pInfo,
                                 int              flags);
static void         FiingerReadInput(InputInfoPtr  pInfo);
static int          FiingerControl(DeviceIntPtr    device,
                                  int             what);

/* internal */
static int _fiinger_init_buttons(DeviceIntPtr device);
static int _fiinger_init_axes(DeviceIntPtr device);

/**
 * Driver Rec, fields are used when device is initialised/removed.
 */
_X_EXPORT InputDriverRec FIINGER = {
    1,
    "fiinger",
    NULL,
    FiingerPreInit,
    FiingerUnInit,
    NULL,
    0
};

/**
 * Module versioning information.
 */
static XF86ModuleVersionInfo FiingerVersionRec =
{
    "fiinger",
    MODULEVENDORSTRING,
    MODINFOSTRING1,
    MODINFOSTRING2,
    XORG_VERSION_CURRENT,
    PACKAGE_VERSION_MAJOR, PACKAGE_VERSION_MINOR, PACKAGE_VERSION_PATCHLEVEL,
    ABI_CLASS_XINPUT,
    ABI_XINPUT_VERSION,
    MOD_CLASS_XINPUT,
    {0, 0, 0, 0}
};

/**
 * Module control. Fields are used when module is loaded/unloaded.
 */
_X_EXPORT XF86ModuleData fiingerModuleData =
{
    &FiingerVersionRec,
    FiingerPlug,
    FiingerUnplug
};


/***************************************************************************
 *                            Module procs                                 *
 ***************************************************************************/


/**
 * Called when module is unloaded.
 */
static void
FiingerUnplug(pointer p)
{
}

/**
 * Called when module is loaded.
 */
static pointer
FiingerPlug(pointer        module,
           pointer        options,
           int            *errmaj,
           int            *errmin)
{
    xf86AddInputDriver(&FIINGER, module, 0);
    return module;
}

/***************************************************************************
 *                            Driver procs                                 *
 ***************************************************************************/

/**
 * Called each time a new device is to be initialized.
 * Init your structs, sockets, whatever else you need to do.
 */
static InputInfoPtr FiingerPreInit(InputDriverPtr  drv, 
                                  IDevPtr         dev, 
                                  int             flags)
{
    InputInfoPtr        pInfo;
    FiingerDevicePtr     pFiinger;
    
    if (!(pInfo = xf86AllocateInput(drv, 0)))
        return NULL;

    pFiinger = xcalloc(1, sizeof(FiingerDeviceRec));
    if (!pFiinger)
    {
        pInfo->private = NULL;
        xf86DeleteInput(pInfo, 0);
        return NULL;
    }
    
    pInfo->private      = pFiinger;

    pInfo->name         = xstrdup(dev->identifier);
    pInfo->flags        = 0;
    pInfo->type_name    = XI_MOUSE; /* see XI.h */
    pInfo->conf_idev    = dev;
    pInfo->read_input   = FiingerReadInput; /* new data avl */
    pInfo->switch_mode  = NULL;            /* toggle absolute/relative mode */
    pInfo->device_control = FiingerControl; /* enable/disable dev */


    /* process driver specific options */
    pFiinger->device = xf86CheckStrOption(dev->commonOptions, 
                                         "Device",
                                         "/dev/scullpipe");
    xf86Msg(X_INFO, "%s: Using device %s.\n", pInfo->name, pFiinger->device);

    /* process generic options */
    xf86CollectInputOptions(pInfo, NULL, NULL);
    xf86ProcessCommonOptions(pInfo, pInfo->options);


    /* Open sockets, init device files, etc. */
    SYSCALL(pInfo->fd = open(pFiinger->device, O_RDWR | O_NONBLOCK));
    if (pInfo->fd == -1)
    {
        xf86Msg(X_ERROR, "%s: failed to open %s.\n", 
                pInfo->name, pFiinger->device);
        pInfo->private = NULL;
        xfree(pFiinger);
        xf86DeleteInput(pInfo, 0);
        return NULL;
    }
    /* do more funky stuff */


    /* close file again */
    close (pInfo->fd);
    pInfo->fd = -1;

    /* set the required flags */
    pInfo->flags |= XI86_OPEN_ON_INIT;
    pInfo->flags |= XI86_CONFIGURED;

    return pInfo;
}

/**
 * Called each time a device is to be removed.
 * Clean up your mess here.
 */
static void FiingerUnInit(InputDriverPtr drv,
                       InputInfoPtr   pInfo,
                       int            flags)
{
    FiingerDevicePtr       pFiinger = pInfo->private;

    if (pFiinger->device)
    {
        xfree(pFiinger->device);
        pFiinger->device = NULL;
    }

    xfree(pFiinger);
    xf86DeleteInput(pInfo, 0);
}

/**
 * Called when data is available on the socket.
 */
static void FiingerReadInput(InputInfoPtr pInfo)
{
    int ir_dots[4][2];

    while(xf86WaitForInput(pInfo->fd, 0) > 0) 
    {
        read(pInfo->fd, ir_dots, 32); //16 is the size in bytes?
	/* now we have all the infos about the 4 ir dots.
	 * lets discover what the user wants to tell us.
	 */
	xf86Msg(X_INFO, "ir_dots[DOT1][X]=%d\n", ir_dots[DOT1][X]);
	xf86Msg(X_INFO, "ir_dots[DOT1][Y]=%d\n", ir_dots[DOT1][Y]);
	xf86Msg(X_INFO, "ir_dots[DOT2][X]=%d\n", ir_dots[DOT2][X]);
	xf86Msg(X_INFO, "ir_dots[DOT2][Y]=%d\n", ir_dots[DOT2][Y]);
	xf86Msg(X_INFO, "ir_dots[DOT3][X]=%d\n", ir_dots[DOT3][X]);
	xf86Msg(X_INFO, "ir_dots[DOT3][Y]=%d\n", ir_dots[DOT3][Y]);
	xf86Msg(X_INFO, "ir_dots[DOT4][X]=%d\n", ir_dots[DOT4][X]);
	xf86Msg(X_INFO, "ir_dots[DOT4][Y]=%d\n", ir_dots[DOT4][Y]);
	
	if(ir_dots[DOT1][X] != -1 && ir_dots[DOT1][Y] != -1 && ir_dots[DOT2][X] == -1 && ir_dots[DOT3][X] == -1 && ir_dots[DOT4][X] == -1) {
		xf86Msg(X_INFO, "Moving\n");
		xf86PostMotionEvent(pInfo->dev,
				1, /* is_absolute */
				0, /* first_valuator */
				2, /* num_valuators */
				ir_dots[DOT1][X],
				ir_dots[DOT1][Y]); /* the coords */
		/* also set free any button that may stay pressed */
		xf86PostButtonEvent(pInfo->dev, 0, BUTTON1, 0, 0, 0); //the second 0 means "release"
		xf86PostButtonEvent(pInfo->dev, 0, BUTTON2, 0, 0, 0); //the second 0 means "release"
		xf86PostKeyEvent(  pInfo->dev, MODIFIER1, 0, 0, 0, 	0); //the first 0 means "release" (ctrl)
		xf86PostKeyEvent(  pInfo->dev, MODIFIER2, 0, 0, 0, 	0); //the first 0 means "release" (alt)
	}
	else {
		/* If two dots are visible, then it's a mouse click.
		* But which one? if the second dot is at left from the first
		* then it's a left click, otherwise it's right.
		*/
		if( ir_dots[DOT1][X] != -1 && ir_dots[DOT2][X] != -1 && ir_dots[DOT3][X] == -1 && ir_dots[DOT4][X] == -1 ) {
			xf86PostMotionEvent(pInfo->dev,1,0,2,ir_dots[DOT1][X],ir_dots[DOT1][Y]);
			//also check if the second dot is not too distant. in this case it will probably be a 
			// modifier the user wants to apply, so ignore it.
			if(ir_dots[DOT1][X] >= ir_dots[DOT2][X] && (ir_dots[DOT1][X] - ir_dots[DOT2][X]) < 200 ) {
				xf86Msg(X_INFO, "Got a left click!\n");
				xf86PostButtonEvent(pInfo->dev,
						    0, //not absolute
						    BUTTON1, //guess what...
						    1, //this means "pressed"
						    0, //first parameter numeber is 0
						    0); //and we got 0 parameter
			}
			else if(ir_dots[DOT1][X] < ir_dots[DOT2][X] && (ir_dots[DOT2][X] - ir_dots[DOT1][X]) < 200) {
				xf86Msg(X_INFO, "Got right click!\n");
				xf86PostButtonEvent(pInfo->dev,
						    0, //not absolute
						    BUTTON2, //as you see
						    1, // "pressed"
						    0, //first valuator
						    0); //number of valuators
			}
			else {
				xf86Msg(X_INFO, "Got two dots, but not a click! so what?!\n");
			}
		}
		else {
			/* this is the case of a mouse click with a modifer applied.
			* the user needs to "produce" first the modifier, then the click.
			*/
			if( ir_dots[DOT1][X] != -1 && ir_dots[DOT2][X] != -1 && ir_dots[DOT3][X] != -1 && ir_dots[DOT4][X] != -1 ) {
				xf86PostMotionEvent(pInfo->dev,1,0,2,ir_dots[DOT1][X],ir_dots[DOT1][Y]);
				//no need to distance check here because we have all 4 dots, so we're sure that a modifier + click
				//is happening. That's why we use the DOT4 this time.
			    
				if(ir_dots[DOT1][X] >= ir_dots[DOT4][X]) {
					xf86Msg(X_INFO, "Got a left click with modifier!\n");
					xf86PostButtonEvent(pInfo->dev,
							    0, //not absolute
							    BUTTON1, //guess what...
							    1, //this means "pressed"
							    0, //first parameter numeber is 0
							    0); //and we got 0 parameter
				}
				else if(ir_dots[DOT1][X] < ir_dots[DOT4][X]) {
					xf86Msg(X_INFO, "Got right click with modifier!\n");
					xf86PostButtonEvent(pInfo->dev,
							    0, //not absolute
							    BUTTON2, //as you see
							    1, // "pressed"
							    0, //first valuator
							    0); //number of valuators
				}
				else {
					xf86Msg(X_INFO, "Got two dots, but not a click! so what?!\n");
				}
			}
			else {
				/* Finally if we can see 3 dots, then the user wants to apply a modifier
				* to the current action. As for mouse click we hold the key untill the
				* dots disappears.
				* If the two dots are aligned orizzontally then ctrl is the modifier.
				* If the two dots are aligned vertically then alt is the modifier.
				* As the user is not a machine, he/she could not be so perfet in
				* in making a 0 difference in some direction for the two dots.
				* So we just consider the higher difference as the intended key.
				*/
				if( ir_dots[DOT1][X] != -1 && ir_dots[DOT2][X] != -1 && ir_dots[DOT3][X] != -1 && ir_dots[DOT4][X] == -1 ) {
					xf86PostMotionEvent(pInfo->dev,1,0,2,ir_dots[DOT1][X],ir_dots[DOT1][Y]);

					if( abs(ir_dots[DOT2][X] - ir_dots[DOT3][X]) > abs(ir_dots[DOT2][Y] - ir_dots[DOT3][Y]) ) {
					    //orizontally aligned
					    xf86Msg(X_INFO, "Got a control modfier\n");
					    xf86PostKeyEvent(  pInfo->dev, //the device
								MODIFIER1, //the key code (ctrl)
								1, //is down
								0, //not absolute
								0, //first valuator
								0); //num valuators
					}
					else {
					    //vertically aligned
					    xf86Msg(X_INFO, "Got a alt modfier\n");
					    xf86PostKeyEvent(  pInfo->dev, //the device
								MODIFIER2, //the key code (alt)
								1, //is down
								0, //not absolute
								0, //first valuator
								0); //num valuators
					}
				}
			  }//third if-else
		      }//second if-else
		}//first if-else	
    }
}

/**
 * Called when the device is to be enabled/disabled, etc.
 * @return Success or X error code.
 */
static int FiingerControl(DeviceIntPtr    device,
                                int             what)
{
    InputInfoPtr  pInfo = device->public.devicePrivate;
    FiingerDevicePtr pFiinger = pInfo->private;

    switch(what)
    {
        case DEVICE_INIT:
            _fiinger_init_buttons(device);
            _fiinger_init_axes(device);
            break;
        /* Switch device on. Establish socket, start event delivery.  */
        case DEVICE_ON:
            xf86Msg(X_INFO, "%s: On.\n", pInfo->name);
            if (device->public.on)
                break;

            SYSCALL(pInfo->fd = open(pFiinger->device, O_RDONLY | O_NONBLOCK));
            if (pInfo->fd < 0)
            {
                xf86Msg(X_ERROR, "%s: cannot open device.\n", pInfo->name);
                return BadRequest;
            }

            xf86FlushInput(pInfo->fd);
            xf86AddEnabledDevice(pInfo);
            device->public.on = TRUE;
            break;
        /**
         * Shut down device.
         */
        case DEVICE_OFF:
            xf86Msg(X_INFO, "%s: Off.\n", pInfo->name);
            if (!device->public.on)
                break;
            xf86RemoveEnabledDevice(pInfo);
            pInfo->fd = -1;
            device->public.on = FALSE;
            break;
        case DEVICE_CLOSE:
            /* free what we have to free */
            break;
    }
    return Success;
}


/***************************************************************************
 *                            internal procs                               *
 ***************************************************************************/

/**
 * Init the button map for the random device.
 * @return Success or X error code on failure.
 */
static int
_fiinger_init_buttons(DeviceIntPtr device)
{
    InputInfoPtr        pInfo = device->public.devicePrivate;
    CARD8               *map;
    int                 i;
    const int           num_buttons = 2;
    int                 ret = Success;

    map = xcalloc(num_buttons, sizeof(CARD8));

    for (i = 0; i < num_buttons; i++)
        map[i] = i;

    if (!InitButtonClassDeviceStruct(device, num_buttons, map)) {
        xf86Msg(X_ERROR, "%s: Failed to register buttons.\n", pInfo->name);
        ret = BadAlloc;
    }

    xfree(map);
    return ret;
}


/**
 * Init the valuators for the random device.
 * Only absolute mode is supported.
 * @return Success or X error code on failure.
 */
static int
_fiinger_init_axes(DeviceIntPtr device)
{
    InputInfoPtr        pInfo = device->public.devicePrivate;
    int                 i;
    const int           num_axes = 2;


    if (!InitValuatorClassDeviceStruct(device,
                                       num_axes,
                                       GetMotionHistory,
                                       GetMotionHistorySize()))
        return BadAlloc;

    pInfo->dev->valuator->mode = Relative;
    if (!InitAbsoluteClassDeviceStruct(device))
        return BadAlloc;

    for (i = 0; i < num_axes; i++)
    {
        xf86InitValuatorAxisStruct(device, i, -1, -1, 1, 1, 1);
        xf86InitValuatorDefaults(device, i);
    }

    return Success;
}

