#include <wiiuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <iostream>
#include <sys/time.h>
#include <sstream>
#include <string>
using namespace std;

#define FRAME_DURATION 10000
#define MAX_WIIMOTES 1
#define DISCOVER_TIMEOUT 10 /* seconds */
#define FOREVER 1
#define WIIMOTE_X_RES 1023;
#define WIIMOTE_Y_RES 767;
#define DOT1 0
#define DOT2 1
#define DOT3 2
#define DOT4 3
#define X 0
#define Y 1

/* Maiusc names should be used only for constant, but these vars
should be threated as constants once the user defines them.
That's why are in maiusc */
int X_RES, Y_RES;
int lastX, lastY;

/**
 * @brief Convert the raw x coord in a screen-aware one
 * 
 * This function convert the raw x coord read from the wiimote
 * to one that fit the screen resolution
 *
 * @param rawX	the raw x coord read from the wiimote
 * @return	the screen-aware x coord
 */
int get_mouse_x(int rawX) {
	int realX;
	realX -= 100; //tries to lower sensitivity in the borders
	realX = (rawX * X_RES) / WIIMOTE_X_RES;
	return( realX );
}


/**
 * @brief Convert the raw y coord in a screen-aware one
 *
 * This function convert the raw y coord read from the wiimote
 * to one that fit the screen resolution
 *
 * @param rawY	the raw y coord read from the wiimote
 * @return	the screen-aware y coord
 */
int get_mouse_y(int rawY) {
	int realY;
	rawY = Y_RES - rawY; //flip the value
	rawY -= 50; //tries to lower sensitivity in the borders
	realY = (rawY * Y_RES) / WIIMOTE_Y_RES;
	return( realY );
}

/**
 * @brief Takes care of all the relevant events that occurs
 *
 * The only relevant events are:
 * dot appear/disappear
 * dot change coords
 *
 * @param wm	The wiimote_t struct describing the wiimote that have generated the event
 */
void handle_event(struct wiimote_t* wm) {
	printf("looping\n");
	if (WIIUSE_USING_IR(wm)) {
		/* Check what ir dots are visible storing their coords.
		those coords will be written to a char device. */
		int ir_dots[4][2], i, j;
		bool moved = false;

		for( i=0; i<4; i++ )
			for( j=0; j<2; j++ )
				ir_dots[i][j] = -1;

		if (wm->ir.dot[DOT1].visible) {
			
			ir_dots[DOT1][X] = get_mouse_x(wm->ir.dot[DOT1].x);
			ir_dots[DOT1][Y] = get_mouse_y(wm->ir.dot[DOT1].y);
			if( abs( ir_dots[DOT1][X] - lastX ) > 10  || abs( ir_dots[DOT1][Y] - lastY ) > 10  ) {
				printf("x=%d\n", ir_dots[DOT1][X] - lastX );
				printf("y=%d\n", ir_dots[DOT1][Y] - lastY );
				lastX = ir_dots[DOT1][X];
				lastY = ir_dots[DOT1][Y];
				moved = true;
			}
			else {
				ir_dots[DOT1][X] = lastX;
				ir_dots[DOT1][Y] = lastY;
			}
		}

		if (wm->ir.dot[DOT2].visible) {
			ir_dots[DOT2][X] = get_mouse_x(wm->ir.dot[DOT2].x);
			ir_dots[DOT2][Y] = get_mouse_y(wm->ir.dot[DOT2].y);
			moved = true;
		}

		if (wm->ir.dot[DOT3].visible) {
			ir_dots[DOT3][X] = get_mouse_x(wm->ir.dot[DOT3].x);
			ir_dots[DOT3][Y] = get_mouse_y(wm->ir.dot[DOT3].y);
			moved = true;
		}

		if (wm->ir.dot[DOT4].visible) {
			ir_dots[DOT4][X] = get_mouse_x(wm->ir.dot[DOT4].x);
			ir_dots[DOT4][Y] = get_mouse_y(wm->ir.dot[DOT4].y);
			moved = true;
		}
		/* now write the matrix of coords in the char device */
		if(moved) {
			FILE *f;
			int i,j;
			for(i=0; i<4;i++) {
			     for(j=0;j<2;j++) {
				    printf("ir_dots[%d][%d] = %d\n",i,j,ir_dots[i][j]);
			      }
			}
			f = fopen("/dev/scullpipe", "a");
			fwrite( ir_dots, sizeof(int), 8, f); //8 is for 4 row x 2 cols
			fclose( f );
		}
	}
}

/**
 * @brief What to do if a wiimote disconnects
 *
 * Prints out a "message" to inform the x11 module.
 * Then the program exits beacuse
 * only one wiimote is supported.
 */
void handle_disconnect(wiimote* wm) {
	printf("\n\n--- DISCONNECTED [wiimote id %i] ---\n", wm->unid);
	exit(0);
}


/**
 * @brief Does nothing, successfully :D
 *
 * Does nothing, successfully :D
 */
void handle_ctrl_status(wiimote* wm) {
	//is called when an expansion is attached to the wiimote.
	//it isn't likely our case...
}

/**
 * @brief Just a useful function to get the time
 *
 * Just a useful function to get the time.
 * I stole this from the Xwii driver.
 */
long int get_time()
{
	static struct timeval mytime;
	gettimeofday(&mytime, NULL);
	long int returnable = (mytime.tv_sec * 1000000) + mytime.tv_usec;
	return returnable;
}

/**
 * @brief A ugly wiimote infrared finger tracking driver.
 * 
 * The purpose of this driver is to identify the ir sources
 * "seen" from the wiimote and make them available "to the public"
 *
 * The wiimote keeps track of a max of 4 ir dots whit their coords
 * mapped in a virtual screen of 1024x768. So this driver also takes
 * care of translating those coords to a user defined resolution.
 */
int main(int argc, char** argv) {
	/* wiimotes array */
	wiimote** wiimotes;
	int found, connected;
	int i = 0;
	lastX = lastY = 0;
	
	/* arguments check and usage info */
	if( !(argc == 3) ) {
		printf("Wiimote InfraRed Finger Tracking \"driver\" v0.0.1alpha\nUsage:\nwirft <x resolution> <y resolution>\n%d",argc);
		return 0;
	}
	X_RES = atoi(argv[1]);
	Y_RES = atoi(argv[2]);
	
	/* Init the wiimotes */
	wiimotes = wiiuse_init(MAX_WIIMOTES);

	/* Try to discover the wiimotes */
	printf("Please put your wiimote(s) in discoverable mode (press 1 and 2 buttons together)\n");
	found = wiiuse_find(wiimotes, MAX_WIIMOTES, DISCOVER_TIMEOUT);
	if (!found) {
		printf("No wiimotes found.\nExiting...\n\n");
		return 0;
	}

	/* Now we have at least one wiimote, so let's connect */
	connected = wiiuse_connect(wiimotes, MAX_WIIMOTES);
	if (connected)
		printf("Connected to %i wiimotes (of %i found).\n", connected, found);
	else {
		printf("Failed to connect to any wiimote.\nExiting...\n\n");
		return 0;
	}

	/* Enable motion sensing and IR tracking for all wiimotes. IR requires motion */
	for(; i < MAX_WIIMOTES; ++i) {
		wiiuse_motion_sensing(wiimotes[i], 1);
		wiiuse_set_ir(wiimotes[i], 1);
	}

	/* A feedback for the user */
	wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1);

	/* start an infinite loop that will take care of what happens */
	long int new_time, old_time = get_time();
	while (FOREVER) {
		new_time = get_time();
		if ((new_time - old_time) > FRAME_DURATION) {
			if (wiiuse_poll(wiimotes, MAX_WIIMOTES)) {
				i = 0;
				for (; i < MAX_WIIMOTES; ++i) {
					switch (wiimotes[i]->event) {

						case WIIUSE_EVENT:
							/* a generic event occured */
							handle_event(wiimotes[i]);
							break;

						case WIIUSE_DISCONNECT:
						case WIIUSE_UNEXPECTED_DISCONNECT:
							/* the wiimote disconnected */
							handle_disconnect(wiimotes[i]);
							return 0;
							break;

						default:
							break;
					} //switch
				} //for
			} //if poll
			old_time = new_time;
		} //if time
	} //while forever


	/* Disconnect the wiimotes */
	wiiuse_cleanup(wiimotes, MAX_WIIMOTES);
	printf("How the hell can we get here?!");
	return 0;
}
